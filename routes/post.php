<?php

use App\Http\Controllers\PostController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'verified'])->group(function () {

    Route::get('/dashboard/posts', [PostController::class, 'index'])
        ->name('dashboard/posts');
    Route::get('/dashboard/posts/create', [PostController::class, 'create'])
        ->name('dashboard/posts/create');
    Route::post('/dashboard/posts', [PostController::class, 'store'])
        ->name('dashboard/posts/store');;
});
