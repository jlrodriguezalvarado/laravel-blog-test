# Laravel Initial Configuration

This project is developed with laravel and sail [https://laravel.com/docs/8.x/installation#getting-started-on-linux]
You need previously install Docker

**Nota**: Se subieron los datos de configuracion del .env en el .env example para ahorrar tiempo de configuracion al ejecutar el proyecto

## Installing Docker

* Docker Engine [https://docs.docker.com/engine/install/]
* Docker compose [https://docs.docker.com/compose/install/]

## Install composer dependencies

run this command to install composer dependencies after clone it

~~~bash
docker run --rm -u "$(id -u):$(id -g)" -v $(pwd):/opt -w /opt laravelsail/php80-composer:latest composer install --ignore-platform-reqs
~~~

## Run Sail

run in root project folder

~~~bash
./vendor/bin/sail up -d
~~~

## Create Database

## Get in to app container

~~~bash
docker exec -it laravel-blog-test_laravel.test_1 bash
~~~

## Install node dependencies

run this command to install node dependencies to work with Laravel Breeze (Inertia with vue)

~~~bash
npm install
~~~

## Compile vue project

~~~bash
npm run dev
~~~

or

~~~bash
npm run prod
~~~

## Config Environment

~~~bash
cp .env.example .env
php artisan key:generate
php artisan migrate
php artisan db:seed
~~~

## Solution

el proyecto se desarrollo con laravel 8 con sistema operativo linux y Docker especificamente trabajando con sail [https://laravel.com/docs/8.x/installation#getting-started-on-linux].
Para el frontend se trabajo con Laravel Breeze [https://laravel.com/docs/8.x/starter-kits#laravel-breeze] especificamente con el stack inertia vue Tailwind.

La seccion orientada al usuario cumple con los requerimientos:

* Que el usuario se pueda registrar
* Que pueda publicar nuevos posts
* Que los usuarios puedan ordenar los posts por fecha de publicacion


Para la solucion de la descarga de post desde otro sitio (sitio del cliente) se creo un comando que a traves de Guzzle y el facade de Http [https://laravel.com/docs/8.x/http-client#introduction] se consulta el endpoint y se insertan los posts en database bajo el usuario admin, se agrego un nivel sencillo de manejo de errores que solo registra un log.
Asi mismo se creo un job [https://laravel.com/docs/8.x/queues#creating-jobs] y una tarea crontab [https://laravel.com/docs/8.x/scheduling#introduction] que ejecuta el job con horizon y redis [https://laravel.com/docs/8.x/horizon#introduction], posteriormente el crontab se tendria que instalar en el servidor el mismo se ejecutaria cada hora a traves de la cola de redis manejada por horizon, esto permitiria que el servidor del cliente no se sature de peticiones y al mismo tiempo la tarea quedaria automatizada.

La configuracion del tiempo de ejecucion de la tarea crontab para la descarga de posts en caso de ser requerido se podria ajustar los tiempos a menor tiempo o mayor esto seria ajustable a nivel del schedule

