<?php

namespace App\Console\Commands;

use App\Models\Post;
use App\Models\User;
use Facade\FlareClient\Http\Exceptions\BadResponse;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GetPostsFromSiteCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:get-from-site';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get posts from client site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $response = Http::get(env('ENDPOINT_POSTS'));

            if ($response->ok()) {
                $user = User::find(1); // Admin

                foreach ($response->json()['data'] as $post) {
                    $post = Post::create($post);
                    $post->user()->associate($user);
                    $post->save();
                }
            }
        } catch (BadResponse $e) {
            Log::info([
                'type'      => 'Error get posts',
                'message'   => $e->getMessage()
            ]);
        } catch (\Exception $e) {
            Log::info([
                'type'      => 'Error get posts',
                'message'   => $e->getMessage()
            ]);
        }
    }
}
