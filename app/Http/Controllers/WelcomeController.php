<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

class WelcomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if ($request->date) {
            $startDate = $request->date . ' 00:00:00';
            $endDate = $request->date . ' 23:59:59';
            $posts = Post::whereBetween('publication_date', [$startDate, $endDate])->paginate(self::PAGINATION);
        } else {
            $posts = Post::paginate(self::PAGINATION);
        }

        return Inertia::render('Welcome', [
            'canLogin'      => Route::has('login'),
            'canRegister'   => Route::has('register'),
            'posts'         => $posts,
            'date'          => $request->date ? $request->date : null
        ]);
    }
}
